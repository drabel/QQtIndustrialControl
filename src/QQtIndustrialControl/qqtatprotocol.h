#ifndef QQTATPROTOCOL_H
#define QQTATPROTOCOL_H


#include <qqtprotocol.h>
#include <qqtcore.h>

#include "qqtindustrialcontrol_global.h"

/**
 * @brief The QQtATMessage class
 * 发送AT命令的报文
 *
 * |head|data|tail|
 * AT+CIMI, ???, \n
 */
class QQTINDUSTRIALCONTROLSHARED_EXPORT QQtATMessage : public QQtMessage
{
    Q_OBJECT
public:
    explicit QQtATMessage ( QObject* parent = 0 ) : QQtMessage ( parent ) {}
    virtual ~QQtATMessage() {}

    // QQtMessage interface
public:
    virtual quint16 minlength() const override {
        return 0x02;
    }
    virtual quint16 maxlength() const override {
        return 0x007F;
    }
    virtual void packer ( QByteArray& l ) const override {
        l << mHead;
        if ( mData.size() > 0 )
            l << "=" << mData;
        l << mTail;
    }
    virtual void parser ( const QByteArray& _l ) override {
        //ignore
    }

private:
    QByteArray mHead;
    QByteArray mData;
    QByteArray mTail;
};

/**
 * @brief The QQtATRecvMessage class
 * 接收 AT命令的报文
 *
 * |head|data|tail|
 * OK+, ???, \n
 */
class QQTINDUSTRIALCONTROLSHARED_EXPORT QQtATRecvMessage : public QQtMessage
{
    Q_OBJECT
public:
    explicit QQtATRecvMessage ( QObject* parent = 0 ) : QQtMessage ( parent ) {}
    virtual ~QQtATRecvMessage() {}

    // QQtMessage interface
public:
    virtual quint16 minlength() const override {
        return 0x02;
    }
    virtual quint16 maxlength() const override {
        return 0x007F;
    }
    virtual void packer ( QByteArray& ) const override {
        //ignore
    }
    virtual void parser ( const QByteArray& _l ) override {
        QByteArray l = _l;

    }

private:
    QByteArray mHead;
    QByteArray mData;
    QByteArray mTail;
};

/**
 * @brief The QQtATProtocol class
 * 串口 AT通信协议
 *
 * 支持打电话、接听电话
 * 发送、读取短信、彩信
 * 操作、读取电话本
 */
class QQTINDUSTRIALCONTROLSHARED_EXPORT QQtATProtocol : public QQtProtocol
{
    Q_OBJECT
public:
    explicit QQtATProtocol ( QObject* parent = 0 ) : QQtProtocol ( parent ) {}
    virtual ~QQtATProtocol() {}

public:
    //发送AT命令

    //发送短信
    void sendMsg();

    //读取短信
    void readMsg();

protected:

public:
    QString errorString();
    int errorCode();

signals:
    void readyReadMsg();
    void readyCalling();
    void readySaying();
    void readyReadPhonon();
    void timeOut(); //ignore, 线路故障概不负责。
    void errorOccured();

    // QQtProtocol interface
public:
    virtual quint16 minlength() override {
    }
    virtual quint16 maxlength() override {
    }
    virtual quint16 splitter ( const QByteArray& ) override {
    }
    virtual bool dispatcher ( const QByteArray& ) override {
    }
    virtual void initializer() override {
    }
};

#endif //QQTATPROTOCOL_H
