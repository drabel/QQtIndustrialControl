##############################################################
#编译 QQtIndustrialControl
#为用户编译本库提供头文件包含集、宏控制集方便。
##############################################################
#头文件
defineTest(add_include_QQtIndustrialControl) {
    header_path=$${PWD}

    command =
    command += $${header_path}

    add_include_path($$command)
    return (1)
}

#本库使用的定义
defineTest(add_defines_QQtIndustrialControl) {

    QT += serialbus

    export(QT)
    export(DEFINES)
    export(CONFIG)
    return (1)
}

#本库使用的定义 - 静态编译时
defineTest(add_static_defines_QQtIndustrialControl) {
    #如果定义编译静态库，那么开启
    DEFINES += QQTINDUSTRIALCONTROL_STATIC_LIBRARY

    add_defines_QQtIndustrialControl()

    export(DEFINES)
    return (1)
}

add_include_QQtIndustrialControl()
contains(DEFINES, LIB_LIBRARY):add_defines_QQtIndustrialControl()
else:contains(DEFINES, LIB_STATIC_LIBRARY):add_static_defines_QQtIndustrialControl()

