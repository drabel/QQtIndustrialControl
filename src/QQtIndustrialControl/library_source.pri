system(touch qqtindustrialcontrol.cpp)

SOURCES +=
HEADERS +=

SOURCES += \
    $$PWD/qqtatprotocol.cpp
HEADERS += \
    $$PWD/qqtatprotocol.h


add_file($$PWD/qqtgpiocontroler.h)
add_file($$PWD/qqtgpiocontroler.cpp)
SOURCES += \
    $$PWD/qqtgpiocontroler.cpp
HEADERS += \
    $$PWD/qqtgpiocontroler.h

#greaterThan(QT_VERSION, 5.8)
add_object_class(QQtModbusManager)
SOURCES += \
    $$PWD/qqtmodbusmanager.cpp
HEADERS += \
    $$PWD/qqtmodbusmanager.h

add_object_class(QQtModbusTcpManager)
SOURCES += \
    $$PWD/qqtmodbustcpmanager.cpp
HEADERS += \
    $$PWD/qqtmodbustcpmanager.h

add_object_class(QQtModbusRtuManager)
SOURCES += \
    $$PWD/qqtmodbusrtumanager.cpp
HEADERS += \
    $$PWD/qqtmodbusrtumanager.h

OBJCLSSS = QQtBuzzerControler
add_object_class($$OBJCLSSS)
SOURCES += \
    $$PWD/$$lower($$OBJCLSSS).cpp
HEADERS += \
    $$PWD/$$lower($$OBJCLSSS).h

OBJCLSSS = QQtCanBusManager
add_object_class($$OBJCLSSS)
SOURCES += \
    $$PWD/$$lower($$OBJCLSSS).cpp
HEADERS += \
    $$PWD/$$lower($$OBJCLSSS).h

SOURCES += \
        qqtindustrialcontrol.cpp

HEADERS += \
        qqtindustrialcontrol.h \
        qqtindustrialcontrol_global.h

include(library_3rdparty.pri)
