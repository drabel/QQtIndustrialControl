#ifndef QQTMODBUSRTUMANAGER_H
#define QQTMODBUSRTUMANAGER_H

#include <QObject>
#include <QModbusRtuSerialMaster>

#include <qqtmodbusmanager.h>
#include <qqtindustrialcontrol_global.h>

/**
 * @brief The QQtModbusRtuManager class
 * 串口总线 MODBUS
 */
class QQTINDUSTRIALCONTROLSHARED_EXPORT QQtModbusRtuManager : public QModbusRtuSerialMaster
{
    Q_OBJECT
public:
    explicit QQtModbusRtuManager ( QObject* parent = 0 );
    virtual ~QQtModbusRtuManager();
    QQtModbusSessionManager* getModbusSessionManager();

    QQtModbusSession* sendRawRequest ( const QModbusRequest& request, int serverAddress );
    QQtModbusSession* sendReadRequest ( const QModbusDataUnit& read, int serverAddress );
    QQtModbusSession* sendWriteRequest ( const QModbusDataUnit& write, int serverAddress );
    QQtModbusSession* sendReadWriteRequest ( const QModbusDataUnit& read, const QModbusDataUnit& write, int serverAddress );

protected:

signals:
    void replyFinished ( QQtModbusSession* session );
    void replyTimeOut ( QQtModbusSession* session );
    void replyErrorOccurred ( QQtModbusSession* session );

public slots:

private slots:
    void errorOccurred ( QModbusDevice::Error error );
    void stateChanged ( QModbusDevice::State state );
    void timeoutChanged ( int newTimeout );

    void localReplyFinished();
    void localReplyTimeOut();
    void localReplyErrorOccurred ( QModbusDevice::Error );

private:
    QQtModbusSessionManager* manager;
};


#endif // QQTMODBUSRTUMANAGER_H

