#ifndef QQTCANBUSMANAGER_H
#define QQTCANBUSMANAGER_H

#include <QObject>
#include <QCanBus>
#include <QCanBusDevice>
#include <QCanBusFrame>

#include <qqtindustrialcontrol_global.h>
/**
 * @brief The QQtCanBusManager class
 * QCanBus管理器
 *
 * 不必封装
 * QCanBus plugin，interface
 * QCanBusDevice readFrame，writeFrame
 * QCanBusFrame frameID
 *
 * 此处提供Manager，用户继承下去，把协议实现在里面即可。
 * void sendXXXCommand(int ...); 里面发送QCanBusFrame；
 * void recvXXXCommand(QCanBusFrame);
 */
class QQTINDUSTRIALCONTROLSHARED_EXPORT QQtCanBusManager : public QObject
{
    Q_OBJECT

public:
    explicit QQtCanBusManager ( QObject* parent = 0 )
        : QObject ( parent ) {}
    virtual ~QQtCanBusManager() {}

    void setPluginName() {}
    void setInterfaceName() {}
    //保证唯一设备 会把先前的句柄删除 至少调用一次
    QCanBusDevice* createDevice() {}

    void sendRequest ( const QCanBusFrame& frame ) {}
    void recvResponse ( const QCanBusFrame& frame ) {}

    /*
     * user interface
     *
     * void sendXXXCommand(...);
     * void recvXXXCommand(const QCanBusFrame& frame);
     */

signals:
    //用户自选上报
    void notifyToBusinessLevel ( const QCanBusFrame& frame );

signals:
    //固定上报
    void readyRead ( const QCanBusFrame& frame );

protected:
    virtual void dispatcher ( const QCanBusFrame& frame ) {
        if ( frame.frameId() == 0x00 )
            recvResponse ( frame );
    }

private slots:
    void slotReadData() {}

private:
    QCanBusDevice* mDev; //device manager？NO
};

#endif // QQTCANBUSMANAGER_H

