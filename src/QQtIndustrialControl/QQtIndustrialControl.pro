#-------------------------------------------------
#
# Project created by QtCreator 2018-06-07T22:23:34
#
#-------------------------------------------------

QT       += widgets

TARGET = QQtIndustrialControl
TEMPLATE = lib

CONFIG += debug_and_release
CONFIG += build_all

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(../../multi-link/add_base_manager.pri)

#本库编译为动态库
contains(DEFINES, LIB_LIBRARY) {
    DEFINES += QQTINDUSTRIALCONTROL_LIBRARY
}else:contains(DEFINES, LIB_STATIC_LIBRARY) {
    DEFINES += QQTINDUSTRIALCONTROL_STATIC_LIBRARY
}

#本库依赖QQt
add_dependent_manager(QQt)

#本库导出SDK到LIB_SDK_ROOT
add_sdk(QQtIndustrialControl, $$add_target_name())
#add_sdk_header_no_postfix(QQtIndustrialControl, $$add_target_name(), QQtIndustrialControl)

include(library_header.pri)
include(library_source.pri)
