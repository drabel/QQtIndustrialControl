#ifndef QQTMODBUSTCPMANAGER_H
#define QQTMODBUSTCPMANAGER_H

#include <QObject>
#include <QModbusTcpClient>

#include <qqtmodbusmanager.h>
#include <qqtindustrialcontrol_global.h>

/**
 * @brief The QQtModbusTcpManager class
 * 网络MODBUS
 */
class QQTINDUSTRIALCONTROLSHARED_EXPORT QQtModbusTcpManager : public QModbusTcpClient
{
    Q_OBJECT
public:
    explicit QQtModbusTcpManager ( QObject* parent = 0 );
    virtual ~QQtModbusTcpManager();
    QQtModbusSessionManager* getModbusSessionManager();

    QQtModbusSession* sendRawRequest ( const QModbusRequest& request, int serverAddress );
    QQtModbusSession* sendReadRequest ( const QModbusDataUnit& read, int serverAddress );
    QQtModbusSession* sendWriteRequest ( const QModbusDataUnit& write, int serverAddress );
    QQtModbusSession* sendReadWriteRequest ( const QModbusDataUnit& read, const QModbusDataUnit& write, int serverAddress );

protected:

signals:
    void replyFinished ( QQtModbusSession* session );
    void replyTimeOut ( QQtModbusSession* session );
    void replyErrorOccurred ( QQtModbusSession* session );

public slots:

private slots:
    void errorOccurred ( QModbusDevice::Error error );
    void stateChanged ( QModbusDevice::State state );
    void timeoutChanged ( int newTimeout );

    void localReplyFinished();
    void localReplyTimeOut();
    void localReplyErrorOccurred ( QModbusDevice::Error );

private:
    QQtModbusSessionManager* manager;
};

#endif // QQTMODBUSTCPMANAGER_H

