#ifndef QQTBUZZERCONTROLER_H
#define QQTBUZZERCONTROLER_H

#include <QObject>

#include <qqtindustrialcontrol_global.h>

QQTINDUSTRIALCONTROLSHARED_EXPORT void buzzer_export();

//PWM周期
QQTINDUSTRIALCONTROLSHARED_EXPORT void buzzer_set_period();
QQTINDUSTRIALCONTROLSHARED_EXPORT void buzzer_get_period ( int* period );

//PWM占空比
QQTINDUSTRIALCONTROLSHARED_EXPORT void buzzer_set_duty_cycle();
QQTINDUSTRIALCONTROLSHARED_EXPORT void buzzer_get_duty_cycle ( int* cycle );

//使能状态
QQTINDUSTRIALCONTROLSHARED_EXPORT void buzzer_set_enable();
QQTINDUSTRIALCONTROLSHARED_EXPORT void buzzer_get_enable();


/**
 * @brief The QQtBuzzerControler class
 * 蜂鸣器控制器
 */
class QQTINDUSTRIALCONTROLSHARED_EXPORT QQtBuzzerControler : public QObject
{
    Q_OBJECT

public:
    explicit QQtBuzzerControler ( QObject* parent = 0 );
    virtual ~QQtBuzzerControler();

    void open();
    void close();

protected:


private:

};

#endif // QQTBUZZERCONTROLER_H

