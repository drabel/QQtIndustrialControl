SOURCES += \
        $${PWD}/qqtdevicesupport.cpp

HEADERS += \
        $${PWD}/qqtdevicesupport.h \
        $${PWD}/qqtdevicesupport_global.h

SOURCES += \
    $${PWD}/qqthiddevice.cpp
HEADERS += \
    $${PWD}/qqthiddevice.h


include($${PWD}/qqtdevicesupport_3rdparty.pri)
