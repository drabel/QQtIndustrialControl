#-------------------------------------------------
#
# Project created by QtCreator 2018-06-07T22:23:34
#
#-------------------------------------------------

QT       += widgets

TARGET = QQtDeviceSupport
TEMPLATE = lib

CONFIG += debug_and_release
CONFIG += build_all

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include($${PWD}/../../multi-link/add_base_manager.pri)

#本库编译为动态库
contains(DEFINES, LIB_LIBRARY) {
    DEFINES += QQTDEVICESUPPORT_LIBRARY
    message(Build $${TARGET} QQTDEVICESUPPORT_LIBRARY is defined. build)
}else:contains(DEFINES, LIB_STATIC_LIBRARY) {
    DEFINES += QQTDEVICESUPPORT_STATIC_LIBRARY
    message(Build $${TARGET} QQTDEVICESUPPORT_STATIC_LIBRARY is defined. build and link)
}


add_version(1,0,0,0)
DEFINES += QQTDEVICESUPPORT_VERSION=$$APP_VERSION

#本库依赖QQt
add_dependent_manager(QQt)

#本库导出SDK到LIB_SDK_ROOT
add_sdk(QQtDeviceSupport, $$add_target_name())
#add_sdk_header(QQtDeviceSupport, $$add_target_name(), QQtDeviceSupport)

include(qqtdevicesupport_header.pri)
include(qqtdevicesupport_source.pri)

CONFIG += continue_build
contains(CONFIG, continue_build){
    system("touch $${PWD}/qqtdevicesupport.cpp")
}

message($${TARGET} build obj dir $$add_host_path($${OUT_PWD}) $$OBJECTS_DIR)
message($${TARGET} build moc dir $$add_host_path($${OUT_PWD}) $$MOC_DIR)
message($${TARGET} build uih dir $$add_host_path($${OUT_PWD}) $$UI_DIR)
message($${TARGET} build rcc dir $$add_host_path($${OUT_PWD}) $$RCC_DIR)
message($${TARGET} build dst dir $$add_host_path($${OUT_PWD}) $$DESTDIR)
message ($${TARGET} QT $${QT})
message ($${TARGET} config $${CONFIG})
message ($${TARGET} define $${DEFINES})
