#ifndef QQTHIDDEVICE_H
#define QQTHIDDEVICE_H

#include "qqtdevicesupport_global.h"

#include <QAbstractSocket>

/**
 * @brief The QQtHidDevice class
 * 这个难度比较高，需要封装hidapi，封装的和QQtUdpSocket差不多。
 */
class QQTDEVICESUPPORTSHARED_EXPORT QQtHidDevice: public QIODevice
{

public:
    explicit QQtHidDevice ( QObject* parent = 0 )
        : QIODevice ( parent ) {

    }

signals:
    void readyRead();
private slots:

};

#endif // QQTHIDDEVICE_H
