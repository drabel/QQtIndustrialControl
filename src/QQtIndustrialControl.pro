TEMPLATE = subdirs
CONFIG += ordered

#有一些模块是继承在源代码里。
#QQtIndustrialControl
SUBDIRS += QQtIndustrialControl

#有一些模块是独立的Library。
#由于QQtIndustrialControl使用Multi-link技术，我会把独立Library发布到他们的SDK目录。

#...
SUBDIRS += QQtDeviceSupport

