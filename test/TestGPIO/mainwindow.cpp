#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qqtgpiocontroler.h>

#define PINBASE 240
#define PIN1 245
#define PIN2 246

MainWindow::MainWindow ( QWidget* parent ) :
    QMainWindow ( parent ),
    ui ( new Ui::MainWindow )
{
    ui->setupUi ( this );

    ui->widget_1->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );
    ui->widget_2->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );
    ui->widget_3->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );
    ui->widget_4->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );
    ui->widget_5->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );
    ui->widget_6->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );

    //使用这个C++公共句柄，干活。
    QQtGPIOControlerV2* inst = QQtGPIOControlerV2::Instance ( this );

    //初始化PIN ID
    inst->export_pin ( PIN1 );
    inst->export_pin ( PIN2 );

    bool ret = false;
    ret = inst->is_exported ( PIN1 );
    qDebug() << "pin:" << quint32 ( PIN1 ) << "is exported:" << ret;
    if ( ret == false )
        return;

    ret = inst->is_exported ( PIN2 );
    qDebug() << "pin:" << quint32 ( PIN2 ) << "is exported:" << ret;
    if ( ret == false )
        return;

    //配置PIN
    inst->configure_pin_direction ( PIN1, true );
    inst->configure_pin_direction ( PIN2, false );
    inst->configure_pin_edge ( PIN1, QQtGPIOControlerV2::EdgeBoth ); //listen有效
    inst->configure_pin_edge ( PIN2, QQtGPIOControlerV2::EdgeNone ); //listen有效

    //test+
    inst->set_value ( PIN1, 0 );
    inst->set_value ( PIN2, 0 );

    connect ( QQtGPIOControlerV2::Instance(), SIGNAL ( readyRead ( uint, int ) ),
              this, SLOT ( slotReadPin ( uint, int ) ) );

    //inst->setTimerInterval ( 1000 );
    //启动监听，监听输入。
    inst->startListen();

    QQtGPIO gpio ( PIN1 );
    qDebug() << ( QQtGPIO& ) gpio;
    QQtGPIO gpio1 ( PIN2 );
    qDebug() << gpio1;

    int value = 0;
    gpio.get_value ( value );
    bool in = false;
    gpio.get_pin_direction ( in );
}

//background-color: rgb(33, 255, 6);
//red background-color: rgb(252, 1, 7);

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotReadPin ( unsigned int pinid, int value )
{
    //业务逻辑，在这里。
    qDebug() << "Test: recv pinid:" << quint32 ( pinid ) << "value:" << qint32 ( value );

    QQtGPIOControlerV2* inst = QQtGPIOControlerV2::Instance ( this );
    if ( pinid == PIN1 )
    {
        //输入，更新状态
        if ( value == 0 )
        {
            ui->widget_1->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );
        }
        else
        {
            ui->widget_1->setStyleSheet ( "QWidget{background-color: rgb(33, 255, 6)}" );
        }

        inst->set_value ( PIN2, value );
        //输出，更新状态
        if ( value == 0 )
        {
            ui->widget_4->setStyleSheet ( "QWidget{background-color: rgb(252, 1, 7)}" );
        }
        else
        {
            ui->widget_4->setStyleSheet ( "QWidget{background-color: rgb(33, 255, 6)}" );
        }
    }
}

void MainWindow::on_pushButton_clicked ( bool checked )
{
    //open
    QQtGPIOControlerV2* inst = QQtGPIOControlerV2::Instance ( this );
    inst->set_value ( PIN1, 1 );
}

void MainWindow::on_pushButton_2_clicked ( bool checked )
{
    //close
    QQtGPIOControlerV2* inst = QQtGPIOControlerV2::Instance ( this );
    inst->set_value ( PIN1, 0 );
}
